package chapter07;

class MyTv3 {
	boolean isPowerOn;
	int channel;
	int volume;
	final int MAX_VOLUME = 100;
	final int MIN_VOLUME = 0;
	final int MAX_CHANNEL = 100;
	final int MIN_CHANNEL = 1;
	int PrevChannel;
	
/*
(1) . 알맞은 코드를 넣어 완성하시오
*/
	
	public int gotoPrevChannel() {
		
	}
	
	public int getChannel() {
		// TODO Auto-generated method stub
		return channel;
	}
	public int getVolume() {
		// TODO Auto-generated method stub
		return volume;
	}
	public void setChannel(int channel) {
		// TODO Auto-generated method stub
		if(channel > MAX_CHANNEL || channel < MIN_CHANNEL) {
			return;
		}
		this.channel = channel;
	}
	public void setVolume(int volume) {
		// TODO Auto-generated method stub
		if(volume > MAX_VOLUME || volume < MIN_VOLUME) {
			return;
		}
		this.volume = volume;
	}
}

class Exercise7_11 {
	public static void main(String args[]) {
		MyTv3 t = new MyTv3();
		t.setChannel(10);
		System.out.println("CH:"+t.getChannel());
		t.setChannel(20);
		System.out.println("CH:"+t.getChannel());	
		t.gotoPrevChannel();
		System.out.println("CH:"+t.getChannel());
		t.gotoPrevChannel();
		System.out.println("CH:"+t.getChannel());
	}
}

